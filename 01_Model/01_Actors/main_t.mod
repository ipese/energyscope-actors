# Main model of the actor cost attribution paper
# May 2023
# The model is run via a R wrapper that loop over the sampling and the actors
# Therefore the model should account only for one optimization of the sampling and a single objective actor

### Definition of the SETS ###

set ACTORS;	# actors without omega
set OMEGA within ACTORS;	# Objective actor
set RES; 	# resources exchanged within the district
set S; 		# system configurations
set E;		# market flows 
set PERIODS; # Periods



### Definition of the VARIABLES ###

var C_omega;
var C_alpha {ACTORS};							# total cost of the iteration
var C_alpha_inv {ACTORS, PERIODS};
var C_alpha_op {ACTORS, PERIODS};
var C_alpha_market {ACTORS, PERIODS};

var C_op{S, RES, ACTORS, ACTORS, PERIODS};

var C_res{S, RES, ACTORS, ACTORS, PERIODS};		# market flow = resources cost
var Y_s{S} binary;						# binary variable selecting the configuration of the system


### Definition of the PARAMETERS ###
param flow{S, RES, ACTORS, ACTORS, PERIODS} default 0;				
param c_flow_market{S, E, ACTORS, PERIODS} default 0;				# inflow from outside the system, OPEX MCHF already
		
param cinv{S,  ACTORS, PERIODS} default 0;					# investment cost by actor

param m_ub{RES} default 100;				# UB 
param m_lb{RES} default 0;				# LB 

param eps{ACTORS} default 0;	# here epsilon is a LHS sample between 0-1  multiplied by the default cost of the actor 

### Definition of the CONSTRAINTS ###
subject to sum_Y_cst:							# Selection of a single configuration of the system
	sum{s in S} (Y_s[s]) = 1;


# Big M constraints
subject to c_res_UB{r in RES, s in S, a in ACTORS, b in ACTORS, t in PERIODS}:
	C_op[s, r, a, b, t] <= Y_s[s] * m_ub[r];

subject to c_res_LB {r in RES, s in S, a in ACTORS, b in ACTORS, t in PERIODS}:
	C_op[s, r, a, b, t] >=  Y_s[s] * m_lb[r];

subject to C_op_constr {r in RES, s in S, a in ACTORS, b in ACTORS, t in PERIODS}:
	C_op[s, r, a, b, t] = C_op[s, r, b, a, t];

subject to EpsilonConstraint{a in ACTORS diff OMEGA}:
	C_alpha[a] <= eps[a];	# Epsilon cst. using LHS sampling

subject to C_alpha_inv_cal {a in ACTORS, t in PERIODS}:
	C_alpha_inv[a, t] = sum{s in S} (Y_s[s] * cinv[s, a, t]);

subject to C_alpha_op_calc {a in ACTORS, t in PERIODS}:
	C_alpha_op[a, t] = sum{s in S} (sum{r in RES} (sum{b in ACTORS} (C_op[s , r, b, a, t] * flow[s, r, b, a, t] - C_op[s , r, a, b, t] * flow[s, r, a, b, t])));

subject to C_alpha_market_calc {a in ACTORS, t in PERIODS}:
	C_alpha_market[a, t] = sum{s in S} (Y_s[s] * sum{e in E} (c_flow_market[s, e, a, t]));

subject to C_alpha_calc{a in ACTORS}:
	C_alpha[a] = sum{t in PERIODS} (C_alpha_inv[a, t] + C_alpha_op[a, t] + C_alpha_market[a, t]);

subject to C_tot_omega_calc {o in OMEGA}:
	C_omega >= C_alpha[o];


### Definition of the OBJECTIVE ###
minimize obj: C_omega;
