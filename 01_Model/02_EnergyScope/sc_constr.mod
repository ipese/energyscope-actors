
param f_sc default 0;
#param a_share {DISTRICTS} default 0;

subject to f_sc_constr:
	(sum {d1 in DISTRICTS} (SC_district[d1]*a_share[d1]))/(sum {d2 in DISTRICTS} (a_share[d2])) * 100 = f_sc;