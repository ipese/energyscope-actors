# Power Shift: Quantifying the Role of Actors in the Multi-Actor Swiss Energy System Decentralisation
## Abstract
The transition towards sustainable energy systems is pivotal in addressing global environmental challenges. Such a shift involves moving from centralized, fossil-fuel-based energy production to decentralized, renewable sources, presenting a complex landscape for energy generation, distribution, and consumption. In this context, Switzerland represents an intriguing case study due to its strategic pivot towards a decentralized, carbon-neutral energy future, emphasizing local renewable energy sources and efficiency. This transition, however, introduces significant challenges in pricing, investments, and managing dynamic energy patterns, necessitating innovative solutions for flexible tariffs, investment models, and the integration of renewable energy sources.\\
Here, we show that optimizing Switzerland's energy system for sustainability, efficiency, and equity requires a nuanced understanding of the interactions among various system actors, including Transmission System Operators (TSOs), Distribution System Operators (DSOs), consumers, and prosumers. Utilizing a Mixed-Integer Linear Programming (MILP) model, our analysis reveals significant trade-offs and the need for strategic balance among these actors, highlighting the role of dynamic pricing models and the spatial and energetic differentiation in prosumer behavior.\\
Compared to previous understandings, our findings underscore the complexity of transitioning to decentralized energy systems and the importance of considering regional variations in system optimization. This study contributes new insights into the economic and strategic considerations driving energy system configurations, offering a comprehensive view of the challenges and opportunities of decentralizing energy production.

The implications of this research extend beyond the Swiss context, providing valuable lessons for global efforts to transition towards sustainable and resilient energy systems. By addressing the intricacies of actor interactions and regional dynamics, this work enhances our understanding of navigating the complexities of modern energy transitions, paving the way for more informed policymaking and system design that align with environmental and societal goals.
## Introduction
This repository contains the model and result data for the corresponding paper.


## Structure

        .
    ├── 01_Model     
    │   ├── 1.1_Optimal_Configurations_and_Actor_weights    # Actors Model        
    │   └── 1.2_The_Role_of_the_Prosumer                    # EnergyScope model (AMPL)                        
    ├── 02_Data                                             # Results data for the corresponding subsections
    │   ├── 3.1_Analysis_of_Configurations 
    │   ├── 3.2_Application_of_Price_Allocation_Method    
    │   ├── 3.3_Optimal_Configurations_and_Actor_weights             
    │   └── 3.4_The_Role_of_the_Prosumer
    └── README.md


## Authors and acknowledgment
- [**Jonas Schnidrig**](mailto:jonas.schnidrig@epfl.ch): Main author
- **Arthur Chuat**: Validation and Review
- **Julia Granacher**: Validation and Review
- **François Maréchal**: Study conceptualization
- **Manuele Margni**: Study conceptualization


## License
MIT Licence

## Project status
Submitted for reviewing
